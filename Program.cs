﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using ShellHelper;

//https://docs.microsoft.com/en-us/dotnet/framework/network-programming/asynchronous-server-socket-example
namespace LinuxController
{
    public class StateObject {  
    public Socket workSocket = null;  
    public const int BufferSize = 1024;  
    public byte[] buffer = new byte[BufferSize];  
    public StringBuilder sb = new StringBuilder();
    public String id = String.Empty;
    }  
    class LinuxController
    {
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        public static ManualResetEvent sendDone = new ManualResetEvent(false);
        public static ManualResetEvent receiveDone = new ManualResetEvent(false);
        public LinuxController() {
        }
        public static void StartListening() {
            //IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            //IPAddress ipaddress = ipHostInfo.AddressList[0];
            IPAddress ipaddress;
            IPAddress.TryParse("192.168.86.30", out ipaddress);
            IPEndPoint localEndPoint = new IPEndPoint(ipaddress, 11000);
            Console.WriteLine($"Local address and port : {localEndPoint.ToString()}"); 

            Socket listener = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try {
                listener.Bind(localEndPoint);
                listener.Listen(100); //max 100 queue 
                while(true){
                    allDone.Reset();
                    Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener
                    );
                    allDone.WaitOne(); //run when allDone.set();
                }
            } catch (Exception e){
                Console.WriteLine(e.ToString());
            }
            //Socket listener 
        }
        public static void AcceptCallback(IAsyncResult ar){
            allDone.Set();
            Socket listener = (Socket) ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            StateObject state = new StateObject();
            state.workSocket = handler;
            Boolean end = false;
            //Receive id from client
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
            receiveDone.WaitOne();
            //Send screen list first (may add auth later for filtering screen list)
            sendScreenList(state);

            while(!end){
                //always receive & use send function after receive complete signal (if required)
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                receiveDone.WaitOne();
            }
        }
        public static void ReadCallback(IAsyncResult ar){
            String content = String.Empty;
            StateObject state = (StateObject) ar.AsyncState;
            Socket handler = state.workSocket;
            int read = handler.EndReceive(ar);
            if(read>0){
                state.sb.Append(Encoding.UTF8.GetString(state.buffer),0,read);
                content = state.sb.ToString();
                if(content.IndexOf("id;") > -1){
                    //receive id
                    String id = content.Split(";")[0];
                    state.id = id;
                    Console.WriteLine($"{state.id} connected!");
                    receiveDone.Set();
                    state.sb = new StringBuilder();
                } else if(content.IndexOf("quit;") > -1){
                    String name = content.Split(";")[0];
                    String quit = "screen -X -S \""+ name +"\" quit";
                    quit.Bash();
                    //Send(handler, $"{name} stopped\n");
                    //sendDone.WaitOne();
                    receiveDone.Set();
                    //sendDone.Reset();
                    //Send(handler, "Updated screen list\n");
                    sendScreenList(state);
                    state.sb = new StringBuilder();
                } else {
                    handler.BeginReceive(state.buffer,0,StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                }
            } else {
                if(state.sb.Length>1){
                    content = state.sb.ToString();
                    Console.WriteLine($"Read {content.Length} bytes from socket.\n Data: {content}");
                }
                handler.Close();
            }
        }
        private static void Send(Socket handler, String data){
            byte[] byteData = Encoding.UTF8.GetBytes(data);
            handler.BeginSend(byteData, 0, byteData.Length,0, new AsyncCallback(SendCallback), handler);
        }
        private static void SendCallback(IAsyncResult ar){
            try {
                Socket handler = (Socket) ar.AsyncState;
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine($"Sent {bytesSent} bytes to client.");
                sendDone.Set();
            } catch (Exception e){
                Console.WriteLine(e.ToString());
            }
        }
        private static void sendScreenList(StateObject state){
            Socket handler = state.workSocket;
            String screenList = "screen -ls";
            String screenListBashFeedback = screenList.Bash();
            String[] screenListArray = screenListBashFeedback.Split(Environment.NewLine);
            String screenListString = String.Empty;
            String pattern = @".*" + state.id + "_.*";
            Regex regex= new Regex(pattern);
            foreach(String screen in screenListArray){
                MatchCollection matches = regex.Matches(screen);
                for(int count = 0; count < matches.Count; count++){
                    screenListString += matches[count].Value + Environment.NewLine;
                }
            }
            screenListString += "\nscreenList;";
            Send(handler,screenListString);
            sendDone.WaitOne();
        }
        static void Main(string[] args){
            StartListening();
            //return 0;
        }
    }
}
